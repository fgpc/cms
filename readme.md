** FGPC Cms **

** Installation **

* Add the following repository to your laravel composer.json:

```
"fgpc/cms": "dev-master"
```


* Add the following code to the bottom of your laravel composer.json:

```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:fgpc/cms.git"
        }
    ]
```

* Run the following command to update your composer.lock file

```
    composer update
```

* Add the ServiceProviders to you application app.php config
    * CmsServiceProvider and LocaleProvider are required!
    
```php
    Framework\Cms\CmsServiceProvider::class,
    Framework\Locale\LocaleServiceProvider::class,
```

* Run the installation command:

```
    php artisan cms:install
```

Follow the installation, Create a master user and add the languages you want.