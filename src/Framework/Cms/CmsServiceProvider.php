<?php

namespace Framework\Cms;

use Framework\Cms\Commands\InstallationCommand;
use Framework\Cms\Http\Middleware\CmsMiddleware;
use Framework\Cms\Menu\MenuButton;
use Framework\Cms\Menu\MenuContainer;
use Framework\Cms\Menu\MenuDropDown;
use Framework\Cms\Menu\MenuList;
use Framework\Cms\Module\ModuleContainer;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider
{

    public function boot(Router $router)
    {
        $this->publishes([
            __DIR__.'/config/permission.php' => config_path('fgpc/permission.php'),
            __DIR__.'/config/role.php' => config_path('fgpc/role.php'),
        ]);

        $this->load();
        $router->aliasMiddleware('cms', CmsMiddleware::class);

        $this->app->singleton('menu', function() {
            return new MenuContainer();
        });
        $this->app->singleton('module', function() {
            return new ModuleContainer();
        });

        if($this->app->runningInConsole()) {
            $this->commands([
                InstallationCommand::class
            ]);
        }

        $this->publishes([
            __DIR__ . '/../../../fonts' => base_path() . '/public/fonts'
        ]);

        $accountMenuList = new MenuList('accounts');

        $accountMenuList->addMenuButton(new MenuButton('users', 'cms.users.index', [], 'users.read'));
        $accountMenuList->addMenuButton(new MenuButton('roles', 'cms.roles.index', [], 'roles.read'));
        $accountMenuList->addMenuButton(new MenuButton('permissions', 'cms.permissions.index', [], 'permissions.read'));

        app('menu')->addMenuList($accountMenuList);

        app('module')->addModule('cms');
    }

    public function register()
    {

    }

    public function load()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . "/resources/views", "cms");
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'cms');
    }

}