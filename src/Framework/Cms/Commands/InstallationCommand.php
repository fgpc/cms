<?php

namespace Framework\Cms\Commands;

use Framework\Cms\Models\Permission;
use Framework\Cms\Models\PermissionLocale;
use Framework\Cms\Models\Role;
use Framework\Cms\Models\RoleLocale;
use Framework\Cms\Models\User;
use Framework\Locale\Models\Locale;
use Framework\Locale\Structure\LocaleStructure;
use Illuminate\Console\Command;

class InstallationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cms installation command';

    public function handle(Locale $locale, LocaleStructure $localeStructure, User $user, Permission $permission, PermissionLocale $permissionLocale, Role $roleModel, RoleLocale $roleLocale)
    {
        $availableLanguages = $this->getAvailableLanguages($localeStructure);
        $selectedLanguages = $this->selectLanguages($availableLanguages);
        $this->addLanguages($locale, $selectedLanguages);

        $locales = $locale->all();
        $this->addDefaultPermissions($permission, $permissionLocale, $locales);
        $permissions = $permission->all()->pluck('id');
        $this->addDefaultRoles($roleModel, $roleLocale, $locales, $permissions);
        $this->addMasterAccount($user, $roleModel);


    }

    private function addDefaultPermissions(Permission $permissionModel, PermissionLocale $permissionLocale, $locales)
    {
        if(isset(config('fgpc.permission')['defaultPermissions'])) {
            $permissions = config('fgpc.permission')['defaultPermissions'];
            foreach($permissions as $key => $permission) {
                if(!$permissionModel->where('slug', $key)->first()) {
                    $newPermission = $permissionModel->create([
                        'slug' => $key
                    ]);
                    foreach($locales as $locale) {
                        if(isset($permission[$locale->code])) {
                            $permissionLocale->create([
                                'name' => $permission[$locale->code],
                                'permission_id' => $newPermission->id,
                                'locale_id' => $locale->id
                            ]);
                        }
                    }
                }
            }
        } else {
            $this->error('The permissions config file is not found!');
            $this->info('Publish the config files of the CmsServiceProvider first!');
            exit;
        }
    }

    private function addDefaultRoles(Role $roleModel, RoleLocale $roleLocale, $locales, $permissions)
    {
        if(isset(config('fgpc.role')['defaultRoles'])) {
            $roles = config('fgpc.role')['defaultRoles'];
            foreach($roles as $key => $role) {
                if(!$roleModel->where('slug', $key)->first()) {
                    $newRole = $roleModel->create([
                        'slug' => $key
                    ]);
                    if($key == 'super_user') {
                        $newPermissions = [];
                        foreach($permissions as $permission) {
                            $newPermissions[$permission] = [
                               'create' => true,
                               'read' => true,
                               'update' => true,
                               'delete' => true,
                            ];
                        }
                        $newRole->permissionRoles()->sync($newPermissions);
                    }
                    foreach($locales as $locale) {
                        if(isset($role[$locale->code])) {
                            $roleLocale->create([
                                'name' => $role[$locale->code],
                                'role_id' => $newRole->id,
                                'locale_id' => $locale->id
                            ]);
                        }
                    }
                }
            }
        } else {
            $this->error('The roles config file is not found!');
            $this->info('Publish the config files of the CmsServiceProvider first!');
            exit;
        }
    }

    public function addMasterAccount(User $user, Role $roleModel)
    {
        $firstName = $this->ask('What is your first name?');
        $lastName = $this->ask('What is your last name?');
        $email = $this->ask('What is your master email?');
        $password = $this->secret('What is you master password?');

        $newUser = $user->firstOrCreate([
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
            'password' => bcrypt($password)
        ]);
        $role = $roleModel->where('slug', 'super_user')->first()->pluck('id');
        $newUser->roles()->attach($role);
    }

    public function getAvailableLanguages(LocaleStructure $localeStructure)
    {
        $languages = [];
        foreach($localeStructure->getAvailableLanguages() as $key => $availableLanguage) {
            $languages[$key] = [
                'name' => $key,
                'code' => $availableLanguage,
                'add' => false
            ];
        }
        return $languages;
    }

    public function getLanguageChoices($availableLanguages)
    {
        $choices = [];
        foreach($availableLanguages as $key => $language) {
            $choices[] = $language['add'] == true ? $key . ' *' : $key;
        }
        $choices[] = 'Exit';
        return $choices;
    }

    public function selectLanguages($availableLanguages)
    {
        $answer = true;
        while($answer != false) {
            $choices = $this->getLanguageChoices($availableLanguages);

            $selectedLanguage = rtrim($this->choice('What language do you want to add? (* are selected languages)', $choices), ' *');
            if($selectedLanguage != 'Exit') {
                $availableLanguages[$selectedLanguage]['add'] = $availableLanguages[$selectedLanguage]['add'] == false ? true : false;

                $headers = ['Language', 'Code', 'Will be added (1)'];
                $this->table($headers, $availableLanguages);
            }
            $answer = $this->confirm('Do you want to add an another one?');
        }
        return $availableLanguages;
    }

    public function addLanguages(Locale $locale, $selectedLanguages)
    {
        $headers = ['Language', 'Code', 'Will be added (1)'];
        $this->table($headers, $selectedLanguages);

        $answer = $this->confirm('Are you sure you want to add the selected languages?', true);
        if($answer) {
            foreach($selectedLanguages as $language) {
                if($language['add'] == true) {
                    $locale->firstOrCreate([
                        'name' => $language['name'],
                        'code' => $language['code']
                    ]);
                }
            }
        }
    }
}
