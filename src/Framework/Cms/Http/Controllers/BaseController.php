<?php

namespace Framework\Cms\Http\Controllers;

use App\Http\Controllers\Controller;
use Framework\Cms\Models\User;

/**
 * Class BaseController
 * @package Framework\Cms\Http\Controllers
 */
abstract class BaseController extends Controller
{
    /**
     * Return 404 if user doesn't has the corrent permission
     *
     * @param $permission
     * @param $type
     * @return bool|void
     */
    protected function abortPermission($permission, $type)
    {
        if (!auth()->user()->hasPermission($permission, $type)) {
            return abort(404);
        }
        return true;
    }

    /**
     * Returns if the current user or the given user is a super user
     *
     * @param User|null $user
     * @return bool
     */
    protected function isSuperUser(User $user = null): bool
    {
        if (!$user) {
            return auth()->user()->hasRole('super_user');
        }

        return $user->hasRole('super_user');
    }
}