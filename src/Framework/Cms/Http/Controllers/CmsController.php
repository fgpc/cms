<?php

namespace Framework\Cms\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;

/**
 * Class CmsController
 * @package Framework\Cms\Http\Controllers
 */
class CmsController extends Controller
{
    /**
     * Returns the cms index view
     * @return Factory|View
     */
    public function index()
    {
        return view('cms::dashboard.index');
    }
}