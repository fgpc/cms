<?php

namespace Framework\Cms\Http\Controllers;

use Framework\Cms\Http\Requests\PermissionRequest;
use Framework\Cms\Models\Permission;
use Framework\Cms\Models\PermissionLocale;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;

/**
 * Class PermissionController
 * @package Framework\Cms\Http\Controllers
 */
class PermissionController extends BaseController
{
    /**
     * Returns the permission index view.
     *
     * @param Permission $permissionModel
     * @return Factory|View
     */
    public function index(Permission $permissionModel)
    {
        $this->abortPermission('permissions', 'read');
        $permissions = $permissionModel->all();
        return view('cms::permissions.index', compact('permissions'));
    }

    /**
     * Returns the permission create view.
     *
     * @return Factory|View
     */
    public function create()
    {
        $this->abortPermission('permissions', 'create');
        return view('cms::permissions.create');
    }

    /**
     * Stores a new Permission into the database.
     * Redirects to the permission index view.
     *
     * @param PermissionRequest $request
     * @param Permission $permissionModel
     * @param PermissionLocale $permissionLocaleModel
     * @return RedirectResponse
     */
    public function store(PermissionRequest $request, Permission $permissionModel, PermissionLocale $permissionLocaleModel)
    {
        $this->abortPermission('permissions', 'create');
        $permission = $permissionModel->create([
            'slug' => str_slug($request->get('name')['en'])
        ]);
        foreach (app('locale')->getLocales() as $locale) {
            if (isset($request->get("name")[$locale->code])) {
                $permissionLocaleModel->create([
                    'name' => $request->get("name")[$locale->code],
                    'permission_id' => $permission->id,
                    'locale_id' => $locale->id
                ]);
            }
        }
        return redirect()->route('cms.permissions.index');
    }

    /**
     * Returns the permission edit view.
     *
     * @param Permission $permission
     * @return Factory|View
     */
    public function edit(Permission $permission)
    {
        $this->abortPermission('permissions', 'update');
        return view('cms::permissions.edit', compact('permission'));
    }

    /**
     * Updates a Permission into the database.
     * Redirects to the permission index view.
     *
     * @param PermissionRequest $request
     * @param Permission $permission
     * @param PermissionLocale $permissionLocaleModel
     * @return RedirectResponse
     */
    public function update(PermissionRequest $request, Permission $permission, PermissionLocale $permissionLocaleModel)
    {
        $this->abortPermission('permissions', 'update');
        $permission->update([
            'slug' => str_slug($request->get('name')['en'])
        ]);
        foreach (app('locale')->getLocales() as $locale) {
            if (isset($request->get("name")[$locale->code])) {
                $permissionLocaleModel->updateOrCreate([
                    'permission_id' => $permission->id,
                    'locale_id' => $locale->id
                ],
                    [
                        'name' => $request->get("name")[$locale->code]
                    ]);
            }
        }
        return redirect()->route('cms.permissions.index');
    }

    /**
     * Removes one permission from the database.
     *
     * @param Permission $permission
     * @return JsonResponse
     */
    public function destroy(Permission $permission)
    {
        $this->abortPermission('permissions', 'delete');
        $permission->delete();
        return response()->json(['message' => true]);
    }

    /**
     * Deletes multiple permissions from the database.
     *
     * @param Request $request
     * @param Permission $permissionModel
     * @return RedirectResponse
     */
    public function delete(Request $request, Permission $permissionModel)
    {
        $this->abortPermission('permissions', 'delete');
        if ($request->has('permissions')) {
            $permissionModel->destroy($request->permissions);
        }
        return redirect()->back();
    }

}