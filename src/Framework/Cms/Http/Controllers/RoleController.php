<?php

namespace Framework\Cms\Http\Controllers;

use Framework\Cms\Http\Requests\RoleRequest;
use Framework\Cms\Models\Permission;
use Framework\Cms\Models\Role;
use Framework\Cms\Models\RoleLocale;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;

/**
 * Class RoleController
 * @package Framework\Cms\Http\Controllers
 */
class RoleController extends BaseController
{
    /**
     * Returns the role index view.
     *
     * @return Factory|View
     */
    public function index()
    {
        if ($this->isSuperUser()) {
            $roles = Role::all();
        } else {
            $roles = Role::where('slug', '!=', 'super_user')->get();
        }
        return view('cms::roles.index', compact('roles'));
    }

    /**
     * Returns the role create view.
     *
     * @return Factory|View
     */
    public function create()
    {
        $this->abortPermission('roles', 'create');
        if ($this->isSuperUser()) {
            $permissions = Permission::all();
        } else {
            $permissions = Permission::where('slug', '!=', 'permissions')->get();
        }
        return view('cms::roles.create', compact('permissions'));
    }

    /**
     * Creates a new role in the database.
     *
     * @param RoleRequest $request
     * @return RedirectResponse
     */
    public function store(RoleRequest $request)
    {
        $this->abortPermission('roles', 'create');
        $role = Role::create([
            'slug' => str_slug($request->get('name')['en'])
        ]);
        foreach (app('locale')->getLocales() as $locale) {
            if (isset($request->get("name")[$locale->code])) {
                RoleLocale::create([
                    'name' => $request->get("name")[$locale->code],
                    'role_id' => $role->id,
                    'locale_id' => $locale->id
                ]);
            }
        }
        if ($request->has('permissions')) {
            if (!$this->isSuperUser()) {
                $permissions = [];
                foreach ($request->permissions as $key => $permission) {
                    $dbPermission = Permission::find($key);
                    if ($dbPermission && $dbPermission->slug != 'permissions') {
                        $permissions[$key] = $permission;
                    }
                }
                $role->permissionRoles()->sync($permissions);
            } else {
                $role->permissionRoles()->sync($request->permissions);
            }
        }

        return redirect()->route('cms.roles.index');
    }

    /**
     * Returns the role edit view.
     *
     * @param Role $role
     * @return Factory|View
     */
    public function edit(Role $role)
    {
        $this->abortPermission('roles', 'update');
        if ($this->isSuperUser()) {
            $permissions = Permission::all();
        } else {
            $permissions = Permission::where('slug', '!=', 'permissions')->get();
        }
        return view('cms::roles.edit', compact('role', 'permissions'));
    }

    /**
     * Updates a role in the database.
     *
     * @param RoleRequest $request
     * @param Role $role
     * @return RedirectResponse
     */
    public function update(RoleRequest $request, Role $role)
    {
        $this->abortPermission('roles', 'update');
        $role->update([
            'slug' => str_slug($request->get('name')['en'])
        ]);
        foreach (app('locale')->getLocales() as $locale) {
            if (isset($request->get("name")[$locale->code])) {
                RoleLocale::updateOrCreate([
                    'role_id' => $role->id,
                    'locale_id' => $locale->id
                ],
                    [
                        'name' => $request->get("name")[$locale->code]
                    ]);
            }
        }
        if ($request->has('permissions')) {
            $permissions = [];
            foreach ($request->permissions as $key => $permission) {
                $dbPermission = Permission::find($key);
                if ($dbPermission && $dbPermission->slug != 'permissions' || $this->isSuperUser()) {
                    $permission['create'] = isset($permission['create']) ? $permission['create'] : 0;
                    $permission['read'] = isset($permission['read']) ? $permission['read'] : 0;
                    $permission['update'] = isset($permission['update']) ? $permission['update'] : 0;
                    $permission['delete'] = isset($permission['delete']) ? $permission['delete'] : 0;
                    $permissions[$key] = $permission;
                }
            }
            $role->permissionRoles()->sync($permissions);
        }
        return redirect()->route('cms.roles.index');
    }

    /**
     * Removes one role from the database.
     *
     * @param Role $role
     * @return JsonResponse
     */
    public function destroy(Role $role)
    {
        $this->abortPermission('roles', 'delete');
        if (!$this->isSuperUser() && $role->slug == 'super_user') {
            abort(404);
        }
        $role->delete();
        return response()->json(['message' => true]);
    }

    /**
     * Removes multiple roles from the database.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $this->abortPermission('roles', 'delete');
        if ($request->has('roles')) {
            if (!$this->isSuperUser()) {
                foreach ($request->roles as $role) {
                    $dbRole = Role::find($role);
                    if (!$dbRole->isSuperUser()) {
                        $dbRole->delete();
                    }
                }
            } else {
                Role::destroy($request->users);
            }
        }
        return redirect()->back();
    }
}