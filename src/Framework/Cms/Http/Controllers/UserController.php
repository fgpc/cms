<?php

namespace Framework\Cms\Http\Controllers;

use Framework\Cms\Http\Requests\DoResetRequest;
use Framework\Cms\Http\Requests\UserRequest;
use Framework\Cms\Http\Requests\LoginRequest;
use Framework\Cms\Http\Requests\ResetRequest;
use Framework\Cms\Models\Role;
use Framework\Cms\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

//TODO CLEANUP!
class UserController extends BaseController
{
    use AuthenticatesUsers;

    protected $redirectTo = '/cms';

    public function showLogin(Request $request)
    {
        return view('cms::login.login');
    }

    public function login(LoginRequest $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function logout()
    {
        $this->guard()->logout();
        return redirect()->route('cms.login');
    }

    public function showRequestReset()
    {
        return view('cms::login.requestReset');
    }

    public function requestReset(ResetRequest $request, User $user, Mailer $mailer, PasswordBroker $broker)
    {
        $response = $broker->sendResetLink(
            $request->only('email')
        );
        if ($response == $broker::RESET_LINK_SENT) {
            return redirect()->route('cms.login')->with(['success' => 'An email has been send!']);
        }
        return redirect()->route('cms.login')->with(['error' => 'Email could not be send please try again']);
    }

    public function showReset(Request $request, $token = null)
    {
        return view('cms::login.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(DoResetRequest $request, PasswordBroker $broker)
    {
        $response = $broker->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'), function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        if ($response == $broker::PASSWORD_RESET) {
            return redirect()->route('cms.index');
        }
        return redirect()->back()
            ->withInput($request->only('email'))
            ->with(['error' => trans($response)]);
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    public function index(User $user)
    {
        if ($this->isSuperUser()) {
            $users = $user->all();
        } else {
            $users = $user->all();
            $users = $users->filter(function ($value, $key) use ($users) {
                return !$this->isSuperUser($value);
            })->all();
        }
        return view('cms::users.index', compact('users'));
    }

    public function create()
    {
        $this->abortPermission('users', 'create');

        if ($this->isSuperUser()) {
            $roles = Role::all();
        } else {
            $roles = Role::where('slug', '!=', 'super_user')->get();
        }
        return view('cms::users.create', compact('roles'));
    }

    public function store(UserRequest $request, User $userModel)
    {
        $this->abortPermission('users', 'create');
        $user = $userModel->create([
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        if ($request->has('roles')) {
            if (!$this->isSuperUser()) {
                $roles = [];
                foreach ($request->roles as $role) {
                    $dbRole = Role::find($role);
                    if ($dbRole && !$dbRole->isSuperUser()) {
                        $roles[] = $dbRole->id;
                    }
                }
                $user->roles()->sync($roles);
            } else {
                $user->roles()->sync($request->roles);
            }
        }
        return redirect()->route('cms.users.index');
    }

    public function edit(User $user)
    {
        $this->abortPermission('users', 'update');
        if ($this->isSuperUser()) {
            $roles = Role::all();
        } else {
            if ($this->isSuperUser($user)) {
                abort(404);
            }
            $roles = Role::where('slug', '!=', 'super_user')->get();
        }
        return view('cms::users.edit', compact('roles', 'user'));
    }

    public function update(UserRequest $request, User $user)
    {
        $this->abortPermission('users', 'update');
        if (!$this->isSuperUser() && $this->isSuperUser($user)) {
            abort(404);
        }
        if (isset($request->password)) {
            $user->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
        } else {
            $user->update($request->except('password'));
        }
        if ($request->has('roles')) {
            if (!$this->isSuperUser()) {
                $roles = [];
                foreach ($request->roles as $role) {
                    $dbRole = Role::find($role);
                    if ($dbRole && !$dbRole->isSuperUser()) {
                        $roles[] = $dbRole->id;
                    }
                }
                $user->roles()->sync($roles);
            } else {
                $user->roles()->sync($request->roles);
            }
        }
        return redirect()->route('cms.users.index');
    }

    public function destroy(User $user)
    {
        $this->abortPermission('users', 'delete');
        if (!$this->isSuperUser() && $this->isSuperUser($user)) {
            abort(404);
        }
        $user->delete();
        return response()->json(['message' => true]);
    }

    public function delete(Request $request)
    {
        $this->abortPermission('users', 'delete');
        if ($request->has('users')) {
            if (!$this->isSuperUser()) {
                foreach ($request->users as $user) {
                    $dbUser = User::find($user);
                    if (!$this->isSuperUser($dbUser)) {
                        $dbUser->destroy();
                    }
                }
            } else {
                User::destroy($request->users);
            }
        }
        return redirect()->back();
    }
}