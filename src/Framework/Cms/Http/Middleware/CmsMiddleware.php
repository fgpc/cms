<?php

namespace Framework\Cms\Http\Middleware;

use Closure;

class CmsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->guest()) {
            return redirect()->route('cms.login');
        }
        return $next($request);
    }
}