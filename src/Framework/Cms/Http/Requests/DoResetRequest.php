<?php

namespace Framework\Cms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoResetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|min:1|max:255|email',
            'password' => 'required|min:6|max:255|confirmed'
        ];
    }
}
