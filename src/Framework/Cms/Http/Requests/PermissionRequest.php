<?php

namespace Framework\Cms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Rule $rule
     * @return array
     */
    public function rules(Rule $rule)
    {
        $permission = $this->route('permission');
        return [
            "name." . $this->getFallbackCode() => 'required',
            "name.en" => 'required',
            "name.*" => [
                'nullable',
                'min:2',
                'max:255',
                isset($permission) ? $rule->unique('permission_locales', 'name')->ignore($permission->id, 'permission_id') : 'unique:permission_locales,name'
            ]
        ];
    }

    public function messages()
    {
        return [
            "name.*.required" => trans('cms::validation.field_required'),
            "name.*.min" => trans('cms::validation.min.string'),
            "name.*.max" => trans('cms::validation.max.string'),
            "name.*.unique" => trans('cms::validation.unique'),
        ];
    }

    private function getFallbackCode()
    {
        return app('locale')->getFallbackLocale()->code;
    }
}
