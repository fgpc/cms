<?php

namespace Framework\Cms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Rule $rule)
    {
        $user = $this->route('user');
        return [
            'firstName' => 'required|min:1|max:255',
            'lastName' => 'required|min:1|max:255',
            'email' => [
                'required',
                'min:1',
                'max:255',
                'email',
                isset($user) ? $rule->unique('users', 'email')->ignore($user->id, 'id') : 'unique:users,email'
            ],
            'password' => [
                !isset($user) ? 'required' : '',
                'nullable',
                'min:6',
                'max:255',
                'confirmed'
            ]
        ];
    }
}
