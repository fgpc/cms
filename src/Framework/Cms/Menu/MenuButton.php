<?php

namespace Framework\Cms\Menu;

use Illuminate\Routing\Route;

/**
 * Class MenuButton
 * @package Framework\Cms\Menu
 */
class MenuButton extends MenuItem
{
    /**
     * @var string
     */
    private $route;

    /**
     * @var array
     */
    private $routeParams = [];

    /**
     * @var string
     */
    private $permission;

    /**
     * MenuItem constructor.
     *
     * @param string $name
     * @param string $route
     * @param array $routeParams
     */
    public function __construct(string $name, string $route, array $routeParams = [], string $permission)
    {
        parent::__construct($name);
        $this->route = $route;
        $this->routeParams = $routeParams;
        if (strpos($this->permission, '.') === false) {
            //Todo throw excepition invalid permissions.
        }
        $this->permission = explode('.', $permission);
    }

    public function getName(): string
    {
        return __($this->name);
    }

    /**
     * Returns the route of the menuItem
     *
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * Returns the full route of the menuItem
     *
     * @return Route
     */
    public function getFullRoute()
    {
        return route($this->route, $this->routeParams);
    }

    /**
     * Returns the route params of the menuItem
     *
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function hasPermission()
    {
        return auth()->user()->hasPermission($this->permission[0], $this->permission[1]);
    }

    /**
     * Returns if the button has children
     * @return bool
     */
    public function hasChildren(): bool
    {
        return false;
    }

    public function getType()
    {
        return 'button';
    }
}