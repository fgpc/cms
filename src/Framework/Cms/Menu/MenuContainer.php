<?php

namespace Framework\Cms\Menu;

/**
 * Class MenuContainer
 * @package Framework\Cms\Menu
 */
class MenuContainer
{
    /**
     * @var array MenuList
     */
    private $menus = [];

    /**
     * Adds a new menu list
     *
     * @param MenuList $menuList
     */
    public function addMenuList(MenuList $menuList)
    {
        $this->menus[$menuList->getSlug()] = $menuList;
    }

    /**
     * Returns the menu lists
     *
     * @return array
     */
    public function getFullMenu()
    {
        return $this->menus;
    }

    /**
     * Returns a menu list by slug
     *
     * @param $slug
     * @return mixed
     */
    public function getMenuListBySlug($slug, $menus = false)
    {
        if(!$menus) {
            $menus = $this->menus;
        }
        foreach($menus as $menu) {
            if($menu instanceof MenuList && $menu->getSlug() == $slug) {
                return $menu;
            }
            if($menu->hasChildren()) {
                return $this->getMenuListBySlug($slug, $menu->getChildren());
            }
        }
        return false;
    }

    public function getMenuButtonBySlug($slug, $menus = false)
    {
        if(!$menus) {
            $menus = $this->menus;
        }
        foreach($menus as $menu) {
            if($menu instanceof MenuButton && $menu->getSlug() == $slug) {
                return $menu;
            }
            if($menu->hasChildren()) {
                return $this->getMenuButtonBySlug($slug, $menu->getChildren());
            }
        }
        return false;
    }

    public function getMenuDropDownBySlug($slug, $menus = false)
    {
        if(!$menus) {
            $menus = $this->menus;
        }
        foreach($menus as $menu) {
            if($menu instanceof MenuDropDown && $menu->getSlug() == $slug) {
                return $menu;
            }
            if($menu->hasChildren()) {
                return $this->getMenuDropDownBySlug($slug, $menu->getChildren());
            }
        }
        return false;
    }
}