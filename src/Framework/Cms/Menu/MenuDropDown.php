<?php

namespace Framework\Cms\Menu;

/**
 * Class MenuDropDown
 * @package Framework\Cms\Menu
 */
class MenuDropDown extends MenuItem
{
    /**
     * MenuDropDown constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function addMenuButton(MenuButton $menuButton)
    {
        $this->children[$menuButton->getSlug()] = $menuButton;
    }

    public function addMenuDropDown(MenuDropDown $menuDropDown)
    {
        $this->children[$menuDropDown->getSlug()] = $menuDropDown;
    }

    public function addMenuList(MenuList $menuList)
    {
        $this->children[$menuList->getSlug()] = $menuList;
    }

    public function getType()
    {
        return 'dropdown';
    }
}