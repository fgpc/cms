<?php

namespace Framework\Cms\Menu;

/**
 * Class MenuItem
 * @package Framework\Cms\Menu
 */
abstract class MenuItem
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var array
     */
    protected $children = [];

    /**
     * MenuItem constructor.
     * @param $slug
     */
    public function __construct(string $slug)
    {
        $this->name = 'cms::menu.' . $slug;
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return __($this->name);
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return bool
     */
    public function hasChildren(): bool
    {
        return !empty($this->children);
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    public abstract function getType();
}