<?php

namespace Framework\Cms\Menu;

/**
 * Class MenuList
 * @package Framework\Cms\Menu
 */
class MenuList extends MenuItem
{
    /**
     * MenuList constructor.
     *
     * @param string $slug
     */
    public function __construct($slug)
    {
        parent::__construct($slug);
    }

    public function addMenuButton(MenuButton $menuButton)
    {
        $this->children[$menuButton->getSlug()] = $menuButton;
    }

    public function addMenuDropDown(MenuDropDown $menuDropDown)
    {
        $this->children[$menuDropDown->getSlug()] = $menuDropDown;
    }

    public function addMenuList(MenuList $menuList)
    {
        $this->children[$menuList->getSlug()] = $menuList;
    }

    public function getType()
    {
        return 'list';
    }
}