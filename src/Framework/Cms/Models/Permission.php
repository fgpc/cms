<?php

namespace Framework\Cms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('permissionLocales', function (Builder $builder) {
            $builder->with('permissionLocales');
        });
    }

    public function permissionLocales()
    {
        return $this->hasMany(PermissionLocale::class);
    }

    public function getPermissionLocaleAttribute()
    {
        $locale = $this->permissionLocales->where('locale_id', app('locale')->getActiveLocale()->id)->first();
        if(!$locale) {
            $locale = $this->permissionLocales->where('locale_id', app('locale')->getFallbackLocale()->id)->first();
        }
        return $locale;
    }

    public function getLocaleName($locale)
    {
        $locale = $this->permissionLocales->where('locale_id', $locale->id)->first();
        return $locale ? $locale->name : '';
    }
}
