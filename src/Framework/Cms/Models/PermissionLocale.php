<?php

namespace Framework\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionLocale extends Model
{
    protected $fillable = [
        'name',
        'permission_id',
        'locale_id'
    ];
}
