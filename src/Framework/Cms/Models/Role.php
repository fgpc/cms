<?php

namespace Framework\Cms\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('roleLocales', function (Builder $builder) {
            $builder->with('roleLocales');
        });
    }

    public function roleLocales()
    {
        return $this->hasMany(RoleLocale::class);
    }

    public function permissionRoles()
    {
        return $this->belongsToMany(Permission::class, 'permission_roles')->withPivot('create', 'read', 'update', 'delete');
    }

    public function getLocaleAttribute()
    {
        $locale = $this->roleLocales->where('locale_id', app('locale')->getActiveLocale()->id)->first();
        if (!$locale) {
            $locale = $this->roleLocales->where('locale_id', app('locale')->getFallbackLocale()->id)->first();
        }
        return $locale;
    }

    public function getLocaleName($locale)
    {
        $locale = $this->roleLocales->where('locale_id', $locale->id)->first();
        return $locale ? $locale->name : '';
    }

    public function hasPermission($id, $field)
    {
        $permission = $this->permissionRoles->find($id);
        return $permission ? $permission->pivot->$field : false;
    }

    public function hasAllPermissionTypes($id)
    {
        $permission = $this->permissionRoles->find($id);
        if(!$permission) {
            return false;
        }
        $pivot = $permission->pivot;
        return $pivot->create && $pivot->read && $pivot->update && $pivot->delete;
    }

    public function hasPermissionBySlug($slug, $type)
    {
        $permission = $this->permissionRoles->where('slug', $slug)->first();
        return $permission ? $permission->pivot->$type : false;
    }

    public function isSuperUser()
    {
        return $this->slug == 'super_user';
    }
}
