<?php

namespace Framework\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class RoleLocale extends Model
{
    protected $fillable = [
        'name',
        'role_id',
        'locale_id'
    ];
}
