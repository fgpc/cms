<?php

namespace Framework\Cms\Module;

class ModuleContainer
{
    private $registeredModules = [];

    public function addModule($moduleName)
    {
        $this->registeredModules[] = $moduleName;
    }

    public function getModules()
    {
        return $this->registeredModules;
    }

    public function isModuleEnabled($moduleName)
    {
        return in_array($moduleName, $this->registeredModules);
    }
}