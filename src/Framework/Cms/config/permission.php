<?php

return [
    'defaultPermissions' => [
        'users' => [
            'nl' => 'Gebruikers',
            'en' => 'Users'
        ],
        'roles' => [
            'nl' => 'Rollen',
            'en' => 'Roles'
        ],
        'permissions' => [
            'nl' => 'Rechten',
            'en' => 'Permissions'
        ],
    ]
];