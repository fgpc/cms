<?php

return [
    'defaultRoles' => [
        'super_user' => [
            'nl' => 'Super user',
            'en' => 'Super user'
        ]
    ]
];