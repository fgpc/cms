/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
require('datatables.net');
require('select2');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

window.addEventListener("orientationchange", function () {
    var $aside = $('aside');
    if (window.screen.width >= 750) {
        $aside.css('display', 'block');
    } else {
        $aside.css('display', 'none');
    }
}, false);

const app = new Vue({
    el: '#app',
    data: {
        lang: ""
    },
    methods: {
        getUnits: function () {

        },
        menuToggle: function (e) {
            e.preventDefault();
            var $ul = $(event.currentTarget).parent().find('ul');
            $ul.slideToggle('active');
            $(e.currentTarget).toggleClass('active');
        },
        hamburgerToggle: function (e) {
            e.preventDefault();
            var $aside = $('aside');
            $aside.toggle({direction: "left"}, 1000);
        },
        deleteItems: function (e) {
            e.preventDefault();
        },
        tabs: function (e, lang) {
            e.preventDefault();
            $('li.active').removeClass('active');
            $(e.currentTarget).parent().addClass('active');
            $('div.tab.active').removeClass('active');
            $('div.' + lang + '-tab').addClass('active');
        },
        deleteButton: function (e, multiple) {
            e.preventDefault();
            if ($(e.currentTarget).hasClass('delete')) {
                if (confirm("Are you sure?")) {
                    var form = null;
                    if (typeof multiple == 'undefined' || multiple != 'true') {
                        var url = $(e.currentTarget).data('url');
                        $.ajax({
                            url: url,
                            method: 'delete',
                            success: function (response) {
                                if (response.message == true) {
                                    location.href = '';
                                }
                            }
                        });
                    } else {
                        form = $(e.currentTarget).closest('form');
                        $(form).submit();
                    }
                } else {
                    $('.delete').removeClass('delete');
                }
            } else {
                $('.delete').removeClass('delete');
                $(e.currentTarget).addClass('delete');
            }
        },
        selector: function (e) {
            var form = $(e.currentTarget).closest('form');
            form.find('input[type="checkbox"]').each(function (key, val) {
                if (key != 0) {
                    val.checked = e.currentTarget.checked;
                }
            });
        },
        rowSelector: function (e) {
            var tr = $(e.currentTarget.closest('tr'));
            tr.find('input[type="checkbox"]').each(function (key, val) {
                val.checked = e.currentTarget.checked;
            });
        }
    },
    mounted() {
        var targets = [0];
        var table = $('#data-table');
        if(table.find('th.delete').length || table.find('th.edit').length) {
            targets.push(-1);
        }
        if(table.find('th.edit').length && table.find('th.delete').length) {
            targets.push(-2);
        }
        table.DataTable({
            info: false,
            lengthChange: false,
            columnDefs: [
                {
                    orderable: false,
                    targets: targets
                }
            ],
            order: [
                2,
                'desc'
            ],
            language: {
                sSearch: '<i class="glyphicon glyphicon-search"></i>',
                emptyTable: '<i class="glyphicon glyphicon-remove"></i>',
                zeroRecords: '<i class="glyphicon glyphicon-remove"></i>',
                paginate: {
                    previous: "«",
                    next: "»"
                }

            },
            fnDrawCallback: function (e) {
                let api = this.api();
                if (api.page.info().pages <= 1) {
                    $('.dataTables_paginate').hide();
                } else {
                    $('.dataTables_paginate').show();
                }
            }
        });
        //TODO check if needed
        // $('form').on('submit', function (e) {
        //     table.rows().nodes().page.len(-1).draw(false);
        // });
        $('.multi-selector').select2({
            theme: "bootstrap",
            width: 'resolve'
        });
        $('body').removeClass('hidden');
    }
});
