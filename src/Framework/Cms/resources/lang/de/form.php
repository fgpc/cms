<?php

return [
    'email' => 'Email',
    'password' => 'Password',
    'forgotPassword' => 'Forgot password?',
    'login' => 'Sign in',
    'backToLogin' => 'Back to login',
    'reset' => 'Request reset'
];