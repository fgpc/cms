<?php

return [
    "accounts" => "Konten",
    "modules" => "Module",
    "users" => "Benutzer",
    "recipe" => "Rezept",
    "recipes" => "Rezepte",
    "sites" => "Standorte",
    "ingredients" => "Zutaten",
    "roles" => "Rollen",
    "units" => "Einheiten",
    "permissions" => "Berechtigungen",
    "pages" => "Seiten",

    "logout" => "Ausloggen"
];