<?php

return [
    'email' => 'Email',
    'firstName' => 'First name',
    'lastName' => 'Last name',
    'password' => 'Password',
    'password_confirm' => 'Password confirmation',
    'roles' => 'Roles',
    'forgotPassword' => 'Forgot password?',
    'login' => 'Sign in',
    'backToLogin' => 'Back to sign in',
    'reset' => 'Request reset',
    'name' => 'Name',
    'created_at' => 'Created at',
];