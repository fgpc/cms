<?php

return [
    "accounts" => "Accounts",
    "modules" => "Modules",
    "users" => "Users",
    "roles" => "Roles",
    "permissions" => "Permissions",
    "logout" => "Sign out"
];