<?php

return [
    'dashboard' => [
        'title' => 'Overview'
    ],
    'login' => [
        'title' => 'Sign in'
    ],
    'users' => [
        'title' => 'Users',
        'single' => 'User',
        'low' => [
            'title' => 'users',
            'single' => 'user'
        ]
    ],
    'permissions' => [
        'title' => 'Permissions',
        'single' => 'Permission',
        'low' => [
            'title' => 'permissions',
            'single' => 'permission'
        ]
    ],
    'roles' => [
        'title' => 'Roles',
        'single' => 'Role',
        'low' => [
            'title' => 'roles',
            'single' => 'role'
        ]
    ],
    'default' => [
        'new_attr' => 'New :attr',
        'new' => 'New',
        'delete_attr' => 'Delete :attr',
        'delete' => 'Delete',
        'destroy' => 'Delete',
        'create' => 'Create',
        'update' => 'Edit',
        'read' => 'Read',
        'edit_attr' => ':attr edit',
        'edit' => 'Edit',
        'all' => 'All',
        'back' => 'Back'
    ]
];