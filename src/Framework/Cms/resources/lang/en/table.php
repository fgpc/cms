<?php

return [
    'name' => 'Name',
    'email' => 'Email',
    'created_at' => 'Created at',
    'edit' => 'Edit',
    'delete' => 'Delete'
];