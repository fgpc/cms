<?php

return [

    'field_required' => 'This field is required',
    'unique' => 'This field already exists',
    'min' => [
        'string' => 'This field has to be a minimum of :min characters'
    ],
    'max' => [
        'string' => 'This field may be :max characters long'
    ],

];
