<?php

return [
    'email' => 'Email',
    'firstName' => 'Voornaam',
    'lastName' => 'Achternaam',
    'password' => 'Wachtwoord',
    'password_confirm' => 'Wachtwoord opnieuw',
    'roles' => 'Rollen',
    'forgotPassword' => 'Wachtwoord vergeten?',
    'login' => 'Log in',
    'backToLogin' => 'Terug naar login',
    'reset' => 'Reset aanvragen',
    'name' => 'Naam',
    'created_at' => 'Aangemaakt op',
];