<?php

return [
    "accounts" => "Accounts",
    "modules" => "Modules",
    "users" => "Gebruikers",
    "roles" => "Rollen",
    "permissions" => "Rechten",
    "logout" => "Log uit"
];