<?php

return [
    'dashboard' => [
        'title' => 'Overzicht'
    ],
    'login' => [
        'title' => 'Login'
    ],
    'users' => [
        'title' => 'Gebruikers',
        'single' => 'Gebruiker',
        'low' => [
            'title' => 'gebruikers',
            'single' => 'gebruiker'
        ]
    ],
    'permissions' => [
        'title' => 'Rechten',
        'single' => 'Recht',
        'low' => [
            'title' => 'rechten',
            'single' => 'recht'
        ]
    ],
    'roles' => [
        'title' => 'Rollen',
        'single' => 'Rol',
        'low' => [
            'title' => 'rollen',
            'single' => 'rol'
        ]
    ],
    'default' => [
        'new_attr' => 'Nieuw :attr',
        'new' => 'Nieuw',
        'delete_attr' => 'Verwijder :attr',
        'delete' => 'Verwijder',
        'destroy' => 'Verwijder',
        'create' => 'Aanmaken',
        'update' => 'Aanpassen',
        'read' => 'Lezen',
        'edit_attr' => ':attr aanpasssen',
        'edit' => 'Aanpassen',
        'all' => 'Alles',
        'back' => 'Terug'
    ]
];