<?php

return [
    'name' => 'Naam',
    'email' => 'Email',
    'created_at' => 'Aangemaakt op',
    'edit' => 'Aanpassen',
    'delete' => 'Verwijder'
];