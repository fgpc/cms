<?php

return [

    'field_required' => 'Dit veld is verplicht',
    'unique' => 'Dit veld bestaat al',
    'min' => [
        'string' => 'Dit veld moet minimaal :min characters bevatten'
    ],
    'max' => [
        'string' => 'Dit veld mag maximaal :max characters bevatten'
    ],

];
