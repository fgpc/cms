<aside class="cms-menu-bar">
    <header>
        <div class="account-wrapper clearfix">
            <a href="{{ route('cms.index') }}"><p class="icon">B</p></a>
            <p class="email">Bart van Venrooij</p>
        </div>
    </header>
    <main>
        <nav>
            <ul>
                @foreach(app('menu')->getFullMenu() as $menu)
                    @include('cms::layouts.menu', ['menuItem' => $menu])
                @endforeach
            </ul>
        </nav>
    </main>
    <footer>
        <div class="logout-wrapper">
            <form action="{{ route('cms.logout') }}" method="POST">
                {{ csrf_field() }}
                <button type="submit">{{ __('cms::menu.logout') }}</button>
            </form>
        </div>
        <div class="copyright-wrapper">
            <p>Copyright by FGPC</p>
        </div>
    </footer>
</aside>