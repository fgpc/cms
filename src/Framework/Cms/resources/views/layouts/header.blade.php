<header class="cms-main-header clearfix">
    <div class="title-wrapper">
        <h1>@yield('title')</h1>
    </div>
    @if(app('module')->isModuleEnabled('locale'))
        <div class="lang-select">
            <nav>
                <ul>
                    <li class="type-dropdown"><a href="" v-on:click="menuToggle($event)">{{ strtoupper(app('locale')->getActiveLocale()->code) }}
                            <span class="caret"></span></a>
                        <ul>
                            @foreach(app('locale')->getLocales() as $lang)
                                <li><a href="{{ route('cms.lang', $lang->code) }}">{{ strtoupper($lang->code) }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    @endif
    <div class="menu">
        <a href="" v-on:click="hamburgerToggle($event)"><i class="glyphicon glyphicon-menu-hamburger"></i></a>
    </div>
</header>