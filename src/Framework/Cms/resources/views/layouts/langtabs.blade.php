<ul class="nav nav-tabs">
    @foreach(app('locale')->getLocales() as $locale)
        <li class="{{ $locale->name == app('locale')->getActiveLocale()->name ? 'active' : '' }}"
            data-lang="{{ $locale->code }}"><a href="#" v-on:click="tabs($event, '{{ $locale->code }}')">{{ $locale->getTranslatedName() }}{{ $locale->code == app('locale')->getFallbackLocale()->code || ($locale->code == 'en' && isset($enRequired)) ? '*' : '' }}</a></li>
    @endforeach
</ul>