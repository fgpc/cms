@if($menuItem->getType() == 'button' && $menuItem->hasPermission())
    <li class="type-button">
        <a href="{{ $menuItem->getFullRoute() }}">{{ $menuItem->getName() }}</a>
    </li>
@else
    @if($menuItem->hasChildren())
        <li class="type-{{ $menuItem->getType() }}">
            @if($menuItem->getType() == 'dropdown')
                <a href="#" v-on:click="menuToggle($event)">{{ $menuItem->getName() }} <span class="caret"></span></a>
            @else
                <p>{{ $menuItem->getName() }}</p>
            @endif
            <ul>
                @foreach($menuItem->getChildren() as $child)
                    @include('cms::layouts.menu', ['menuItem' => $child])
                @endforeach
            </ul>
        </li>
    @endif
@endif