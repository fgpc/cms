<header class="login-header clearfix">
    @if(app('module')->isModuleEnabled('locale'))
        <div class="lang-select">
            <nav>
                <ul>
                    <li class="type-dropdown"><a href="" v-on:click="menuToggle($event)">{{ app('locale')->getActiveLocale()->code }}
                            <span class="caret"></span></a>
                        <ul>
                            @foreach(app('locale')->getLocales() as $lang)
                                <li><a href="{{ route('cms.lang', $lang->code) }}">{{ strtoupper($lang->code) }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    @endif
</header>