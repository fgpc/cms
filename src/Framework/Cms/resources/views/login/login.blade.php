@extends('cms::login.master')

@section('title', __('cms::page.login.title'))

@section('content')
    <main>
        <div class="login-wrapper">
            @if(session()->has('success'))
                <div class="alert alert-success" role="alert">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session()->get('error') }}
                </div>
            @endif
            <form action="{{ route('cms.login') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">{{ __('cms::form.email') }}</label>
                    <input type="email" name="email" id="email" class="form-control">
                    @include('cms::errors.validation', ['field' => 'email'])
                </div>
                <div class="form-group">
                    <label for="password">{{ __('cms::form.password') }}</label>
                    <input type="password" name="password" id="password" class="form-control">
                    @include('cms::errors.validation', ['field' => 'password'])
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="{{ __('cms::form.login') }}">
                    <a href="{{ route('cms.showRequestReset') }}"
                       class="pull-right">{{ __('cms::form.forgotPassword') }}</a>
                </div>
            </form>
        </div>
    </main>
@stop