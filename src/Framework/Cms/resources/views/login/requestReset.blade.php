@extends('cms::login.master')

@section('title', __('cms::form.reset'))

@section('content')
    <main>
        <div class="login-wrapper">
            <form action="{{ route('cms.requestReset') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">{{ __('cms::form.email') }}</label>
                    <input type="email" name="email" id="email" class="form-control">
                    @include('cms::errors.validation', ['field' => 'email'])
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="{{ __('cms::form.reset') }}">
                    <a href="{{ route('cms.showLogin') }}" class="pull-right">{{ __('cms::form.backToLogin') }}</a>
                </div>
            </form>
        </div>
    </main>
@stop