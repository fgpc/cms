@extends('cms::login.master')

@section('title', __('cms::form.reset'))

@section('content')
    <main>
        <div class="login-wrapper">
            <form action="{{ route('cms.reset') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token or '' }}">
                <div class="form-group">
                    <label for="email">{{ __('cms::form.email') }}</label>
                    <input type="email" name="email" id="email" class="form-control">
                    @include('cms::errors.validation', ['field' => 'email'])
                </div>
                <div class="form-group {{ $errors->has("password") ? 'has-error' : '' }}">
                    <label for="password">{{ __('cms::form.password') }}</label>
                    <input type="password" name="password" id="password" class="form-control">
                    @include('cms::errors.validation', ['field' => "password"])
                </div>

                <div class="form-group {{ $errors->has("password_confirmation") ? 'has-error' : '' }}">
                    <label for="password_confirmation">{{ __('cms::form.password_confirm') }}</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                    @include('cms::errors.validation', ['field' => "password_confirmation"])
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="{{ __('cms::form.reset') }}">
                    <a href="{{ route('cms.showLogin') }}" class="pull-right">{{ __('cms::form.backToLogin') }}</a>
                </div>
            </form>
        </div>
    </main>
@stop

