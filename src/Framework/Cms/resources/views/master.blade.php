<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ elixir('vendor/cms/css/app.css') }}">
</head>
<body class="hidden">


<div id="app">
    @include('cms::layouts.header')
    @include('cms::layouts.aside')
    @yield('content')
</div>

<script src="{{ elixir('vendor/cms/js/app.js') }}"></script>
</body>
</html>