@extends('cms::master')

@section('title', __('cms::page.default.edit'))

@section('content')
    <div class="content">
        <a href="{{ route('cms.permissions.index') }}" class="btn btn-default">{{ __('cms::page.default.back') }}</a>
        <section class="language-tab">
            @include('cms::layouts.langtabs', ['enRequired' => true])
            <form action="{{ route('cms.permissions.update', $permission->id) }}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                @include('cms::permissions.partials.form')
                <button class="btn btn-default">{{ __('cms::page.default.update') }}</button>
            </form>

        </section>
    </div>
@stop