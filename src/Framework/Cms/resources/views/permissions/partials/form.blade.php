@foreach(app('locale')->getLocales() as $locale)
    <div class="tab {{ $locale->code }}-tab {{ $locale->name == app('locale')->getActiveLocale()->name ? 'active' : '' }}">
        <div class="form-group {{ $errors->has("name.$locale->code") ? 'has-error' : '' }}">
            <label for="name">{{ __('cms::form.name') }}{{ $locale->code == app('locale')->getFallbackLocale()->code || $locale->code == 'en' ? '*' : '' }}
            </label>
            <input type="text" name="name[{{ $locale->code }}]" id="name" class="form-control"
                   value="{{  !old("name.$locale->code") && isset($permission) ? $permission->getLocaleName($locale)  : old("name.$locale->code")}}">
            @include('cms::errors.validation', ['field' => "name.$locale->code"])
        </div>
    </div>
@endforeach