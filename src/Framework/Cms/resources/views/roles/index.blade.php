@extends('cms::master')

@section('title', __('cms::page.roles.title'))

@section('content')
    <div class="content">
        @if(auth()->user()->hasPermission('roles', 'create'))
            <a href="{{ route('cms.roles.create') }}"
               class="btn btn-default pull-left">{{ __('cms::page.default.new') }}</a>
        @endif
        <form action="{{ route('cms.roles.delete') }}" method="POST">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            @if(auth()->user()->hasPermission('roles', 'delete'))
                <div class="clearfix">
                    <button class="btn btn-default pull-right"
                            v-on:click="deleteButton($event, 'true')">{{ __('cms::page.default.delete') }}
                    </button>
                </div>
            @endif
            <div class="table-wrapper">
                <table id="data-table" class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="1"><input type="checkbox" v-on:change="selector($event)"></th>
                        <th>{{ __('cms::table.name') }}</th>
                        <th>{{ __('cms::table.created_at') }}</th>
                        @if(auth()->user()->hasPermission('roles', 'update'))
                            <th width="1" class="edit">{{ __('cms::table.edit') }}</th>
                        @endif
                        @if(auth()->user()->hasPermission('roles', 'delete'))
                            <th width="1" class="delete">{{ __('cms::table.delete') }}</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($roles as $role)
                        <tr>
                            <td><input type="checkbox" name="roles[]" value="{{ $role->id }}"></td>
                            <td>{{ $role->locale->name }}</td>
                            <td>{{ $role->created_at }}</td>
                            @if(auth()->user()->hasPermission('roles', 'update'))
                                <td><a href="{{ route('cms.roles.edit', $role->id) }}" class="edit-icon"><i
                                                class="glyphicon glyphicon-pencil"></i></a></td>
                            @endif
                            @if(auth()->user()->hasPermission('roles', 'delete'))
                                <td>

                                    <button type="submit" data-url="{{ route('cms.roles.destroy', $role->id) }}"
                                            class="delete-icon"
                                            v-on:click="deleteButton($event)"><i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </form>
    </div>
@stop