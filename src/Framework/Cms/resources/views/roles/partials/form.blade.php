@foreach(app('locale')->getLocales() as $locale)
    <div class="tab {{ $locale->code }}-tab {{ $locale->name == app('locale')->getActiveLocale()->name ? 'active' : '' }}">
        <div class="form-group {{ $errors->has("name.$locale->code") ? 'has-error' : '' }}">
            <label for="name">{{ __('cms::form.name') }}{{ $locale->code == app('locale')->getFallbackLocale()->code || $locale->code == 'en' ? '*' : '' }}
            </label>
            <input type="text" name="name[{{ $locale->code }}]" id="name" class="form-control"
                   value="{{  !old("name.$locale->code") && isset($role) ? $role->getLocaleName($locale)  : old("name.$locale->code")}}">
            @include('cms::errors.validation', ['field' => "name.$locale->code"])
        </div>
    </div>
@endforeach

<table class="table">
    <thead>
    <tr>
        <th>{{ __('cms::page.permissions.title') }}</th>
        <th>{{ __('cms::page.default.create') }}</th>
        <th>{{ __('cms::page.default.read') }}</th>
        <th>{{ __('cms::page.default.update') }}</th>
        <th>{{ __('cms::page.default.destroy') }}</th>
        <th>{{ __('cms::page.default.all') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($permissions as $permission)
        <tr>
            <td>{{ $permission->permission_locale->name }}</td>
            <td><input type="checkbox" name="permissions[{{ $permission->id }}][create]"
                       {{ !old("permissions.$permission->id.create") && isset($role) && $role->hasPermission($permission->id, 'create') ? 'checked' : old("permissions.$permission->id.create") == '1' ? 'checked' : '' }} value="1">
            </td>
            <td><input type="checkbox" name="permissions[{{ $permission->id }}][read]"
                       {{ !old("permissions.$permission->id.read") && isset($role) && $role->hasPermission($permission->id, 'read') ? 'checked' : old("permissions.$permission->id.read") == '1' ? 'checked' : '' }} value="1">
            </td>
            <td><input type="checkbox" name="permissions[{{ $permission->id }}][update]"
                       {{ !old("permissions.$permission->id.update") && isset($role) && $role->hasPermission($permission->id, 'update') ? 'checked' : old("permissions.$permission->id.update") == '1' ? 'checked' : '' }} value="1">
            </td>
            <td><input type="checkbox" name="permissions[{{ $permission->id }}][delete]"
                       {{ !old("permissions.$permission->id.delete") && isset($role) && $role->hasPermission($permission->id, 'delete') ? 'checked' : old("permissions.$permission->id.delete") == '1' ? 'checked' : '' }} value="1">
            </td>
            <td><input type="checkbox" name="all[{{ $permission->id }}]"
                       v-on:click="rowSelector($event)" {{ isset($role) && $role->hasAllPermissionTypes($permission->id) ? 'checked' : old("all.$permission->id") ? 'checked' : '' }}>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>