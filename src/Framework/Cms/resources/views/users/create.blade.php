@extends('cms::master')

@section('title', __('cms::page.default.new'))

@section('content')
    <div class="content">
        <a href="{{ route('cms.users.index') }}" class="btn btn-default">{{ __('cms::page.default.back') }}</a>
        <section class="language-tab">
            <form action="{{ route('cms.users.store') }}" method="POST">
                {{ csrf_field() }}
                @include('cms::users.partials.form')
                <button class="btn btn-default">{{ __('cms::page.default.create') }}</button>
            </form>

        </section>
    </div>
@stop