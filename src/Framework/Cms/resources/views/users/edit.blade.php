@extends('cms::master')

@section('title', __('cms::page.default.edit'))

@section('content')
    <div class="content">
        <a href="{{ route('cms.users.index') }}" class="btn btn-default">{{ __('cms::page.default.back') }}</a>
        <section class="language-tab">
            <form action="{{ route('cms.users.update', $user->id) }}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                @include('cms::users.partials.form')
                <button class="btn btn-default">{{ __('cms::page.default.update') }}</button>
            </form>

        </section>
    </div>
@stop