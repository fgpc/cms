<div class="form-group {{ $errors->has("firstName") ? 'has-error' : '' }}">
    <label for="firstName">{{ __('cms::form.firstName') }}</label>
    <input type="text" name="firstName" id="firstName" class="form-control"
           value="{{  !old("firstName") && isset($user) ? $user->firstName  : old("firstName")}}">
    @include('cms::errors.validation', ['field' => "firstName"])
</div>

<div class="form-group {{ $errors->has("lastName") ? 'has-error' : '' }}">
    <label for="lastName">{{ __('cms::form.lastName') }}</label>
    <input type="text" name="lastName" id="lastName" class="form-control"
           value="{{  !old("lastName") && isset($user) ? $user->lastName  : old("lastName")}}">
    @include('cms::errors.validation', ['field' => "lastName"])
</div>

<div class="form-group {{ $errors->has("email") ? 'has-error' : '' }}">
    <label for="email">{{ __('cms::form.email') }}</label>
    <input type="email" name="email" id="email" class="form-control"
           value="{{  !old("email") && isset($user) ? $user->email  : old("email")}}">
    @include('cms::errors.validation', ['field' => "email"])
</div>

<div class="form-group {{ $errors->has("password") ? 'has-error' : '' }}">
    <label for="password">{{ __('cms::form.password') }}</label>
    <input type="password" name="password" id="password" class="form-control">
    @include('cms::errors.validation', ['field' => "password"])
</div>

<div class="form-group {{ $errors->has("password_confirmation") ? 'has-error' : '' }}">
    <label for="password_confirmation">{{ __('cms::form.password_confirm') }}</label>
    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
    @include('cms::errors.validation', ['field' => "password_confirmation"])
</div>

<div class="form-group {{ $errors->has("roles") ? 'has-error' : '' }}">
    <label for="roles">{{ __('cms::form.roles') }}</label>
    <select name="roles[]" id="roles" class="form-control multi-selector" multiple="multiple">
        @foreach($roles as $role)
            @if(old('roles'))
                {{ debug(old('roles')) }}
            @endif
            <option value="{{ $role->id }}" {{ !old('roles') && isset($user) && $user->roles->where('id', $role->id)->first() ? 'selected' : old('roles') && in_array($role->id, old('roles')) ? 'selected' : '' }}>{{ $role->locale->name }}</option>
        @endforeach
    </select>
    @include('cms::errors.validation', ['field' => "roles"])
</div>

