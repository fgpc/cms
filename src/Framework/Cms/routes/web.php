<?php

Route::group(['namespace' => 'Framework\Cms\Http\Controllers', 'middleware' => ['web', 'lang'], 'prefix' => 'cms'], function() {
    Route::get('login', "UserController@showLogin")->name('cms.showLogin');
    Route::get('request/reset', "UserController@showRequestReset")->name('cms.showRequestReset');
    Route::get('reset/{token}', "UserController@showReset")->name('cms.showReset');
    Route::post('reset', "UserController@reset")->name('cms.reset');
    Route::post('request/reset', "UserController@requestReset")->name('cms.requestReset');
    Route::post('login', "UserController@login")->name('cms.login');
    Route::group(['middleware' => 'cms'], function () {
        Route::post('logout', "UserController@logout")->name('cms.logout');
        Route::get('/', "CmsController@index")->name('cms.index');

        Route::resource('users', "UserController", ['as' => 'cms']);
        Route::delete('cms/users/delete', "UserController@delete")->name('cms.users.delete');

        Route::resource('roles', "RoleController", ['as' => 'cms']);
        Route::delete('cms/roles/delete', "RoleController@delete")->name('cms.roles.delete');

        Route::resource('permissions', "PermissionController", ['as' => 'cms']);
        Route::delete('cms/permissions/delete', "PermissionController@delete")->name('cms.permissions.delete');
    });
});
