<?php

namespace Framework\Locale\Http\Controllers;

use App\Http\Controllers\Controller;

class LocaleController extends Controller
{
    public function lang($lang)
    {
        if($lang != session('locale') && app('locale')->getLocale($lang)) {
            session()->put('locale', $lang);
        }
        return redirect()->back();
    }

}