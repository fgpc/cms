<?php

namespace Framework\Locale\Http\Middleware;

use Closure;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('locale')) {
            session()->put('locale', app()->getLocale());
        }
        app()->setLocale(session('locale'));
        return $next($request);
    }
}