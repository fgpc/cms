<?php

namespace Framework\Locale;

use Framework\Locale\Http\Middleware\LocaleMiddleware;
use Framework\Locale\Models\Locale;
use Framework\Locale\Structure\LocaleStructure;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class LocaleServiceProvider extends ServiceProvider
{

    public function boot(Router $router, Locale $locale)
    {
        $this->load();
        $router->aliasMiddleware('lang', LocaleMiddleware::class);

        $this->app->singleton('locale', function() use ($locale) {
            return new LocaleStructure($locale);
        });

        app('module')->addModule('locale');
    }

    public function register()
    {
    }

    public function load()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }

}