<?php

namespace Framework\Locale\Models;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    protected $fillable = [
        'name',
        'code'
    ];

    public function getTranslatedName()
    {
        return __("cms::locales.$this->code");
    }
}
