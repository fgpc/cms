<?php

namespace Framework\Locale\Structure;

use Framework\Locale\Models\Locale;
use Illuminate\Support\Collection;

class LocaleStructure
{
    private $locales;

    private $availableLanguages = [
        'Dutch' => 'nl',
        'English' => 'en',
        'German' => 'de'
    ];

    public function __construct(Locale $locale)
    {
        $locales = $locale->all();
        if($locales->isEmpty()) {
            $locales = new Collection();
            $fallbackLocale = new Locale();
            $fallbackLocale->setAttribute('code', app()->getLocale());
            $locales->push($fallbackLocale);
        }
        $this->locales = $locales;
    }

    public function getLocale($langCode)
    {
        return $this->locales->where('code', strtolower($langCode))->first();
    }

    public function getActiveLocale()
    {
        $locale = $this->locales->where('code', strtolower(app()->getLocale()))->first();
        return !$locale ? $this->locales : $locale;
    }

    public function getLocales()
    {
        return $this->locales;
    }

    public function getFallbackLocale()
    {
        return $this->locales->where('code', config('app.fallback_locale'))->first();
    }

    public function getAvailableLanguages()
    {
        return $this->availableLanguages;
    }
}