<?php

Route::group(['namespace' => 'Framework\Locale\Http\Controllers', 'middleware' => ['web'], 'prefix' => 'cms'], function() {
    Route::get('lang/{lang}', 'LocaleController@lang')->name('cms.lang');
});