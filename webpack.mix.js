const { mix } = require('laravel-mix');
let copy = require('copy');


const srcPath = "src/Framework/";
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(srcPath + 'Cms/resources/assets/js/app.js', srcPath + 'Cms/public/js')
    .sass(srcPath + 'Cms/resources/assets/sass/app.scss', srcPath + 'Cms/public/css');

mix.copy(srcPath + 'Cms/public/js', '../../../public/vendor/cms/js', false);
mix.copy(srcPath + 'Cms/public/css', '../../../public/vendor/cms/css', false);